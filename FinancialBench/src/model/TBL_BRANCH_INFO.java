package model;
/**
 * 
 * @author qqkang
 *
 */
public class TBL_BRANCH_INFO {

	String branch_id;//varchar(20), primary key
	String branch_name;//varchar(5)
	String valid_state;//char(1), 0-valid, 1-invalid
	String branch_street;//varchar(100)
	String branch_city;//varchar(50)
	String branch_nation;//varchar(50)
}
