package model;
/**
 * 
 * @author qqkang
 *
 */
public class TBL_MCHNT_INFO {

	String mchnt_cd;//char(10), primary key
	String mchnt_name;// varchar(20)
	String mchnt_tp;//char(1)
	String branch_id;//varchar(20)
	String mchnt_street;//varchar(150)
	String mchnt_city;//varchar(100)
	String mchnt_nation;//varchar(100)
	String memo;//varchar(150)
}
