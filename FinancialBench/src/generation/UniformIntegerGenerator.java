/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package generation;
/**
 *
 * @author DELL
 */
public class UniformIntegerGenerator extends Generator {
    //private Powernum pnum=new Powernum();
    private String stra[]; //Just for test
    public UniformIntegerGenerator(){}
    public UniformIntegerGenerator(int n,int m)
    {
        super(n,m);
        //For test
        stra=new String[5];
        stra[0]="China";
        stra[1]="USA";
        stra[2]="UK";
        stra[3]="Japan";
        stra[4]="Indian";
    }
    @Override
    public int NumRun() {
        int numout=0;
        numout=min+(int)((max-min)*(Math.random()));
        return numout;
    }
    @Override
    public String StringRun() {
        int startstr=0;
        int endstr=stra.length-1;
        int strnum=0;
        strnum=startstr+(int)((endstr-startstr)*(Math.random()));
        return stra[strnum];
    }
}
