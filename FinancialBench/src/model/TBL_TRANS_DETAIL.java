package model;
/**
 * 
 * @author qqkang
 *
 */
public class TBL_TRANS_DETAIL {

	int trans_record_no;//交易纪录号
	String card_bin;
	String card_attr;
	String card_media;
	String iss_ins_id;
	String acq_ins_id;
	String fwd_ins_id;
	String rcv_ins_id;
	String trans_type;
	int trans_amount;
	String trans_chnl;
	String mchnt_cd;//char(10)
	String mchnt_tp;//char(1)
	String term_id;
	String term_seq;
	String pos_entry_md_cd;
	String pos_cond_cd;
	String branch_id;//varchar(20)
	String pri_acct_no;
	String respCd;//char(2)
	String datetime;//date
	String time;
}
