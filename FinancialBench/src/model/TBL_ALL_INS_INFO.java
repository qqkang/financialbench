package model;
/**
 * 
 * @author qqkang
 *
 */
public class TBL_ALL_INS_INFO {

	String ins_id;//char(8), primary key
	String ins_name;//varchar(50)
	String ins_type;//char(1)
	String ins_street;//varchar(100)
	String ins_city;//varchar(50)
	String ins_nation;//varchar(50)
	String ins_memo;//varchar(100)
}
