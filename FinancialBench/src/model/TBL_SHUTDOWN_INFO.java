package model;

import java.sql.Timestamp;

/**
 * 
 * @author qqkang
 *
 */
public class TBL_SHUTDOWN_INFO {

	int rec_id;//int
	String ins_id;//char(8)
	String ins_type;//char(1)
	String event_msg;//varchar(150)
	Timestamp begin_ts;//timestamp
	Timestamp end_ts;//timestamp
	String down_rsn;//varchar(150)
	String span;//varchar(50)
}
