/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package generation;
/**
 *
 * @author DELL
 */
public class SkewedIntegerGenerator extends Generator {
    //private Powernum pnum=new Powernum();
    double skewedratio;
    int sdata;
    public SkewedIntegerGenerator(){}
    public SkewedIntegerGenerator(int n,int m,double r)
    {
        super(n,m);
        //sdata:skewed line of the data
        sdata=(max-min)/2;
        //The skewed rate of skewed data
        skewedratio=r;
    }
    @Override
    public int NumRun() {
        int numout=0;
        double s;
         //java.util.Random r=new java.util.Random();
        //numout=min+(max-min)*r.nextInt();
        s=Math.random();
        if (s<this.skewedratio) 
            numout=min+(int)((this.sdata-min)*(Math.random()));
        else
            numout=this.sdata+(int)((this.sdata-min)*(Math.random()));
        return numout;
    }

    @Override
    public String StringRun() {
        throw new UnsupportedOperationException("Not supported yet.");
    }


}
