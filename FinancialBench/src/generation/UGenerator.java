/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package generation;
import java.io.*;
import java.util.Scanner;
/**
 *
 * @author DELL
 */
public class UGenerator {
    private int min,max,num;
    public UGenerator()
    {
        min=0;
        max=0;
        num=0;
    }
    private void ugIntegerWorker() throws IOException
    {
        UniformIntegerGenerator unif=new UniformIntegerGenerator(min,max);
        File f1= new File("output/output.txt") ;
        f1.delete();
        f1.createNewFile();
        for (int i=0;i<num;i++)
        {
           this.ugOutputToFile(f1,unif.NumRun());
        }
        System.out.println();
    }
    private void ugStringWorker() throws IOException
    {
        UniformStringGenerator unif=new UniformStringGenerator();
        File f1= new File("output/output.txt") ;
        f1.delete();
        f1.createNewFile();
        for (int i=0;i<num;i++)
        {
           this.ugOutputToFile(f1,unif.StringRun());
        }
        System.out.println();
    }
    private void ugIntegerInput() throws IOException
    {
        int min1=0,max1=0,num1=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the min!");
        min1=sc.nextInt();
        System.out.println("Please enter the max!");
        max1=sc.nextInt();
        System.out.println("Please enter the num!");
        num1=sc.nextInt();

        //set the min,max,num
        this.setNums(min1, max1, num1);
    }
     private void ugStringInput() throws IOException
    {
        int num1=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the num!");
        num1=sc.nextInt();
        //set the num
        this.setNums(0, 0, num1);
    }
    private void ugOutputToFile(File f1,int n) throws IOException
    {
        FileWriter w = new FileWriter(f1,true);
        w.write(String.valueOf(n));
        w.write("\r\n");
        w.close();
    }
    private void ugOutputToFile(File f1,String str) throws IOException
    {
        FileWriter w = new FileWriter(f1,true);
        w.write(str);
        w.write("\r\n");
        w.close();
    }
    private void setNums(int n,int m,int k)
    {
        min=n;
        max=m;
        num=k;
    }
    public void ugIntegerRunner() throws IOException
    {
        this.ugIntegerInput();
        this.ugIntegerWorker();
    }
    public void ugStringRunner() throws IOException
    {
        this.ugStringInput();
        this.ugStringWorker();
    }
}
