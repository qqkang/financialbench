/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package generation;
/**
 *
 * @author DELL
 */
public class SkewedStringGenerator extends Generator {
    private String stra[]; //Just for test
    private double skewedratio;
    private String skewedstra[];
    public SkewedStringGenerator()
    {
        super();
        skewedratio=0.0;     //Set in the program for test
        //For test
        stra=new String[5];
        stra[0]="China";
        stra[1]="USA";
        stra[2]="UK";
        stra[3]="Japan";
        stra[4]="Indian";
    }
    private void setSkewedStringArray()
    {
        //For test
        skewedstra=new String[3];
        skewedstra[0]="China";
        skewedstra[1]="Japan";
        skewedstra[2]="USA";
    }
    @Override
    public String StringRun() {
        if (stra.length<0) return null;
        this.setSkewedStringArray(); //set skewed string data
        int startstr=0;
        int endstr=stra.length;
        int strnum=0;
        double s=0;
        s=Math.random();
        if (s<this.skewedratio)
        {
            endstr=skewedstra.length;
            strnum=startstr+(int)Math.ceil((endstr-startstr)*(Math.random()))-1;
            return skewedstra[strnum];
        }
        else
        {
            strnum=startstr+(int)Math.ceil((endstr-startstr)*(Math.random()))-1;
            return stra[strnum];
        }
    }

    @Override
    public int NumRun() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
