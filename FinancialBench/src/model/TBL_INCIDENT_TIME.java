package model;

import java.sql.Timestamp;

/**
 * 
 * @author qqkang
 *
 */
public class TBL_INCIDENT_TIME {

	int rec_id;//int
	String ins_id;//char(8)
	String ins_type;//char(1)
	String event_msg;//varchar(150)
	Timestamp begin_ts;//timestamp
	Timestamp end_ts;//timestamp
	String msg;//varchar(150)
}
