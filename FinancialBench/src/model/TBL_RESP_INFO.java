package model;
/**
 * 
 * @author qqkang
 *
 */
public class TBL_RESP_INFO {

	String respCd;//char(2)
	String resp_name;//varchar(20)
	String resp_desc;//varchar(100)
	String resp_type;//varchar(50)
	String valid_state;//char(2)
}
