/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package generation;
import java.io.*;
/**
 *
 * @author DELL
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        //平均分布
        UGenerator ug= new UGenerator();
        ug.ugIntegerRunner();       //Int平均分布
        ug.ugStringRunner();        //String平均分布，String字段写死了

        //倾斜分布
        SGenerator sg= new SGenerator();
        sg.sgIntegerRunner();       //Int倾斜分布分布，需倾斜值，默认倾斜数值为（max-min）/2到max之间的数
        sg.sgStringRunner();        //String倾斜分布，String字段、倾斜字段、倾斜值已设定
    }
}
