/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package generation;

import java.io.*;
import java.util.Scanner;

/**
 *
 * @author DELL
 */
public class SGenerator {
    private int min,max,num;
    private double skewedratio;
    public SGenerator()
    {
        min=0;
        max=0;
        num=0;
        skewedratio=0.0;
    }
    private void sgIntegerWorker() throws IOException
    {
        SkewedIntegerGenerator skewed=new SkewedIntegerGenerator(min,max,skewedratio);
        File f1= new File("../Generator0.1/output/output.txt") ;
        f1.delete();
        f1.createNewFile();
        for (int i=0;i<num;i++)
        {
           this.sgOutputToFile(f1,skewed.NumRun());
        }
        System.out.println();
    }
    private void sgStringWorker() throws IOException
    {
        SkewedStringGenerator skewed=new SkewedStringGenerator();
        File f1= new File("../Generator0.1/output/output.txt") ;
        f1.delete();
        f1.createNewFile();
        for (int i=0;i<num;i++)
        {
           this.sgOutputToFile(f1,skewed.StringRun());
        }
        System.out.println();
    }
    private void sgIntegerInput() throws IOException
    {
        int min1=0,max1=0,num1=0;
        double ratio1=0.0;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter the min!");
        min1=sc.nextInt();
        System.out.println("Please enter the max!");
        max1=sc.nextInt();
        System.out.println("Please enter the num!");
        num1=sc.nextInt();
        System.out.println("Please enter the skewedratio!");
        ratio1 = sc.nextDouble();

        //set the min,max,num
        this.setNums(min1, max1, num1,ratio1);
    }
    private void sgStringInput() throws IOException
    {
        int min1=0,max1=0,num1=0;
        double ratio1=0.0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the num!");
        num1=sc.nextInt();
        //System.out.println("Please enter the skewedratio!");
        //ratio1 = sc.nextDouble();

        //set the min,max,num
        this.setNums(0, 0, num1,ratio1);
    }
    private void sgOutputToFile(File f1,int n) throws IOException
    {
        FileWriter w = new FileWriter(f1,true);
        w.write(String.valueOf(n));
        w.write("\r\n");
        w.close();
    }
    private void sgOutputToFile(File f1,String str) throws IOException
    {
        FileWriter w = new FileWriter(f1,true);
        w.write(str);
        w.write("\r\n");
        w.close();
    }
    private void setNums(int n,int m,int k,double s)
    {
        min=n;
        max=m;
        num=k;
    }
    public void sgIntegerRunner() throws IOException
    {
        this.sgIntegerInput();
        this.sgIntegerWorker();
    }
    public void sgStringRunner() throws IOException
    {
        this.sgStringInput();
        this.sgStringWorker();
    }

}
